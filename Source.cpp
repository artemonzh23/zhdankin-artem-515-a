#include <stdio.h>
#include <stdlib.h>

int main()
{
	float a, b, c, d, x, y, z, px, py, pz, t;
	printf("Projection of a point onto a plane\n");
	printf("Enter plane coefficients\n");
	printf(" ax+by+cz+d=0\n");
	printf("Enter a: ");
	scanf_s("%f", &a);
	printf("Enter b: ");
	scanf_s("%f", &b);
	printf("Enter c: ");
	scanf_s("%f", &c);
	printf("Enter d: ");
	scanf_s("%f", &d);
	printf("Enter the coordinates of the point\n");
	printf("Enter x: ");
	scanf_s("%f", &x);
	printf("Enter y: ");
	scanf_s("%f", &y);
	printf("Enter z: ");
	scanf_s("%f", &z);
	t = (-(a*x) - (b*y) - (c*z) - d) / (a * a + b * b + c * c);
	px = a * t + x;
	py = b * t + y;
	pz = c * t + z;
	printf("Point projection coordinates\n");
	printf("x: %.4f  ", px);
	printf("y: %.4f  ", py);
	printf("z: %.4f\n", pz);
	system("pause");
	return 0;
}
